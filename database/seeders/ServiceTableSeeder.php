<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Service;

class ServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $service = [
            [
                'id'         => 1,
                'service'     => 'First Service',
                
            ],
            
             [
                'id'         => 2,
                'service'     => 'Second Service',
                    
            ],
            [
                 'id'         => 3,
                 'service'     => 'Third Service',
                        
            ],
        ];
        Service::insert($service);

    }
}
