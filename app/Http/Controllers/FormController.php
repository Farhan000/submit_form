<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Form;
use App\Models\Country;
use App\Models\Service;


class FormController extends Controller
{

    public function show(){
        $country = Country::select('id','name')->get();
        $service = Service::select('id','service')->get();

        return view('submitform',compact('country','service'));
    
}
public function table(){

    $data = Form::all();

    return view('list',['forms'=>$data]);

}
  

public function store(Request $request){
      
      if($file = $request->file('file')){
          $name= $file->getClientOriginalName();
          if($file->move('file_name',$name)){
        $form = new form;
       $form->name = $request->name;
       $form->email = $request->email;
       
       $form->country = $request->country;
       $form->phone_no = $request->phone_no;
       $form->service = $request->service;
       $form->description = $request->description;
      
       $form->file_name = $name;
       $form->save();

       return redirect('data')->with('success','data saved');
          }
      }


    }


}
