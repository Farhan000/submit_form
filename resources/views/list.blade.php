<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        
      
        <title>Form</title>
    </head>

    <body>
<div style="margin-left:35%; margin-top:2%;">
<h1>Submitted Form Information</h1>
</div>
<div class="container" style="margin-top:4%">
<table class="table table-dark table-striped">
<thead>
    <tr>
        <th scope="col">Id</th>
        <th scope="col">Name</th>
        <th scope="col">Email</th>
        <th scope="col">Country</th>
        <th scope="col">Phone no</th>
        <th scope="col">Service</th>
        <th scope="col">Description</th>
        <th scope="col">File Name</th>
    </tr>
    </thead>
    <tbody>
    @foreach($forms as $form)
    <tr>
        <td>{{$form['id']}}</td>
        <td>{{$form['name']}}</td>
        <td>{{$form['email']}}</td>
        <td>{{$form['country']}}</td>
        <td>{{$form['phone_no']}}</td>
        <td>{{$form['service']}}</td>
        <td>{{$form['description']}}</td>
        <td>{{$form['file_name']}}</td>
    </tr>
    @endforeach
    </tbody>
</table>
</div>

</body>
</html>